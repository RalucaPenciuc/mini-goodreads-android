package com.example.minigoodreads.auth.data

data class TokenHolder(
    val token: String
)
