package com.example.minigoodreads.auth.data

data class User(
    val username: String,
    val password: String
)
