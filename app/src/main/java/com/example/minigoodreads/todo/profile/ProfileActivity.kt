package com.example.minigoodreads.todo.profile

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.example.minigoodreads.R
import com.example.minigoodreads.todo.books.BookListFragment
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.charts.PieChart
import kotlinx.android.synthetic.main.profile_fragment.*

class ProfileActivity : Activity() {
    private var readBooks = 0
    private var nonReadBooks = 0
    private lateinit var pieChart: PieChart
    private lateinit var pieData: PieData
    private lateinit var pieDataSet: PieDataSet
    private lateinit var pieEntries: ArrayList<PieEntry>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_fragment)

        pieChart = pie_chart

        readBooks = intent.getIntExtra("read", 0)
        nonReadBooks = intent.getIntExtra("not read", 0)
        pieEntries = ArrayList()
        pieEntries.add(PieEntry(readBooks.toFloat(), "read"))
        pieEntries.add(PieEntry(nonReadBooks.toFloat(), "not read"))

        pieDataSet = PieDataSet(pieEntries, "")
        pieData = PieData(pieDataSet)
        pieChart.data = pieData
        pieDataSet.setColors(*ColorTemplate.JOYFUL_COLORS)
        pieDataSet.sliceSpace = 2f
        pieDataSet.valueTextColor = Color.WHITE
        pieDataSet.valueTextSize = 10f
        pieDataSet.sliceSpace = 5f
    }
}
