package com.example.minigoodreads.todo.books

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.minigoodreads.R
import com.example.minigoodreads.auth.data.AuthRepository
import com.example.minigoodreads.core.TAG
import com.example.minigoodreads.todo.profile.ProfileActivity
import kotlinx.android.synthetic.main.book_list_fragment.*
import com.example.minigoodreads.todo.book.BookEditFragment

class BookListFragment : Fragment() {
    private lateinit var bookListAdapter: BookListAdapter
    private lateinit var bookListModel: BookListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.v(TAG, "onCreateView")
        return inflater.inflate(R.layout.book_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.v(TAG, "onActivityCreated")
        if (!AuthRepository.isLoggedIn) {
            findNavController().navigate(R.id.login_fragment)
            return
        }
        setupBookList()
        fab.setOnClickListener {
            Log.v(TAG, "add new book")
            findNavController().navigate(R.id.book_edit_fragment)
        }
        chartButton.setOnClickListener {
            Log.v(TAG, "go to chart activity")
            val intent = Intent(activity, ProfileActivity::class.java)
            var readBooks = 0
            var nonReadBooks = 0
            bookListAdapter.books.forEach { book ->
                run {
                    if (book.read) {
                        readBooks += 1
                    }
                    else if (!book.read) {
                        nonReadBooks += 1
                    }
                }
            }
            intent.putExtra("read", readBooks)
            intent.putExtra("not read", nonReadBooks)
            activity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            startActivity(intent)
        }
    }

    private fun setupBookList() {
        bookListAdapter = BookListAdapter(this)
        book_list.adapter = bookListAdapter
        bookListModel = ViewModelProviders.of(this).get(BookListViewModel::class.java)
        bookListModel.books.observe(this, Observer { books ->
            Log.v(TAG, "update books")
            bookListAdapter.books = books
        })
        bookListModel.loading.observe(this, Observer { loading ->
            Log.v(TAG, "update loading")
            progress.visibility = if (loading) View.VISIBLE else View.GONE
        })
        bookListModel.loadingError.observe(this, Observer { exception ->
            if (exception != null) {
                Log.v(TAG, "update loading error")
                val message = "Loading exception ${exception.message}"
                val parentActivity = activity?.parent
                if (parentActivity != null) {
                    Toast.makeText(parentActivity, message, Toast.LENGTH_SHORT).show()
                }
            }
        })
        bookListModel.refresh()
    }
}
