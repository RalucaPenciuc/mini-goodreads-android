package com.example.minigoodreads.todo.books

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.minigoodreads.R
import com.example.minigoodreads.todo.book.BookEditFragment
import com.example.minigoodreads.todo.data.Book
import kotlinx.android.synthetic.main.book_view.view.*

class BookListAdapter(private val fragment: Fragment) : RecyclerView.Adapter<BookListAdapter.ViewHolder>() {

    var books = emptyList<Book>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private var onBookClick: View.OnClickListener

    init {
        onBookClick = View.OnClickListener { view ->
            val book = view.tag as Book
            fragment.findNavController().navigate(R.id.book_edit_fragment, Bundle().apply {
                putString(BookEditFragment.BOOK_ID, book._id)
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.book_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val book = books[position]
        holder.textView.text = book.title
        holder.itemView.tag = book
        holder.itemView.setOnClickListener(onBookClick)
    }

    override fun getItemCount() = books.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.text
    }
}
