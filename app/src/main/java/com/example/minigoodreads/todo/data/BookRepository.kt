package com.example.minigoodreads.todo.data

import androidx.lifecycle.LiveData
import com.example.minigoodreads.todo.data.local.BookDao
import com.example.minigoodreads.core.Result
import com.example.minigoodreads.todo.data.remote.BookApi

class BookRepository(private val bookDao: BookDao) {

    val books = bookDao.getAll()

    suspend fun refresh(): Result<Boolean> {
        return try {
            val books = BookApi.service.find()
            for (book in books) {
                bookDao.insert(book)
            }
            Result.Success(true)
        } catch(e: Exception) {
            Result.Error(e)
        }
    }

    fun getById(bookId: String): LiveData<Book> {
        return bookDao.getById(bookId)
    }

    suspend fun save(book: Book): Result<Book> {
        return try {
            book._id = book.author + "fjhbd" + book.title
            val createdBook = BookApi.service.create(book)
            bookDao.insert(createdBook)
            Result.Success(createdBook)
        } catch(e: Exception) {
            Result.Error(e)
        }
    }

    suspend fun update(book: Book): Result<Book> {
        return try {
            val updatedBook = BookApi.service.update(book._id, book)
            bookDao.update(updatedBook)
            Result.Success(updatedBook)
        } catch(e: Exception) {
            Result.Error(e)
        }
    }
}